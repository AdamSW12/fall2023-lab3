package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void echo_return5(){
        assertEquals("Testing that the echo method returns 5", 5, App.echo(5));
    }

    @Test
    public void oneMore_Returns6(){
        assertEquals("Testing oneMore returns 6 when given 5", 6, App.oneMore(5));
    }
}
